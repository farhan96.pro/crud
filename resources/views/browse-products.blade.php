<center>
	<h1>Here is the list of all products </h1>

	<a href="{{route('product.create')}}"><button>Add new Product</button></a>

	<table border="2px">
		<tr>
			<th>Product Name</th>
			<th> Product Standard</th>
			<th> Product Details</th>
			<th>Edit product</th>
			<th>delete product</th>



		</tr>
		@foreach($product as $pro)

		<tr>
			<td>{{$pro->name}}</td>
			<td>{{$pro->standard}}</td>
			<td>{{$pro->details}}</td>

			<th> <a href="{{ route('product.edit',[$pro->id],'edit') }}">Edit</a></th>
			<th>
			 	<form method="post" action="{{ route('product.destroy',$pro->id) }}">
			 		@csrf
			 		@method('DELETE')
			 		<button type="submit">Delete</button>
			 	</form>
			 	
			</th>
			<!-- <th> <a href="">delete</a></th> -->

		</tr>
		@endforeach
	</table>

</center>

<script type="text/javascript">
	

</script>